<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WebsiteController extends Controller
{
    public function index()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        // $datas = DB::table('tabel_banks')->select('id','bank_name','bank_code','account_name',
        //     'account_number','created_at','updated')->get();

        return view('beranda/index');
    }

    public function ourcompany()
    {
        return view('about/index');
    }

    public function portfolio()
    {
        $datas = DB::select("select * from our_project where active = 1 order by id_project DESC");

        return view('portfolio/index', compact(['datas']));
    }
}
