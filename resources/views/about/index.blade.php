@extends('layouts.app')
<title>Our Company - Jayabaya Technology</title>
@section('content')
   
    @php
        $client = DB::select("select * from idclient order by id_client DESC");
    @endphp

            <section>
                <div class="w-100">
                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric125" data-source="gallery" style="background-color:transparent;padding:0px;">
                        <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;background-color: #b8dfff ;background-position: center;background-size: cover;background-repeat: no-repeat;" data-version="5.4.1">
                            <ul>
                                <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/about.png" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['-20','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['20','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp3.png" alt="Slide Mockup 3">
                                    </div>
 -->
                                    <!-- LAYER NR. 4 -->
                                    
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff; ">Our Company
                                    </div>
                                </li>
                                <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/about.png" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['110','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['180','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-50','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp3.png" alt="Slide Mockup 3">
                                    </div> -->

                                    <!-- LAYER NR. 4 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff;">About Us
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!-- END REVOLUTION SLIDER -->
                </div>
            </section>
            <section>
                <div class="w-100 pt-40 pb-100 position-relative">
                    <div id="particles1" class="particles-js top-left" data-color="#ffe27a" data-saturation="300" data-size="40" data-count="8" data-speed="2" data-hide="770" data-image="assets"></div>
                    <div class="container">
                        <div class="about-wrap5 w-100">
                            <div class="row align-items-center">
                                <div class="col-md-5 col-sm-12 col-lg-5 order-md-1">
                                    <img class="img-fluid mt-40" src="assets/MetaMax/assets/images/resources/about2.png" alt="About Mockup 4">
                                </div>
                                <div class="col-md-7 col-sm-12 col-lg-7">
                                    <div class="about-desc5 w-100 mt-70">
                                        <h2 class="mb-0">About Our</h2>
                                        <br>
                                         <h5 class="mb-0">Jayabaya Technology is a company engaged in IT (Information Technology) and has a home base in Kediri, East Java.</h5>
                                         <br>
                                         <h5 class="mb-0">In the first years of our establishment, Jayabaya Technology has been trusted by clients from various regions in Indonesia, especially on the islands of Java, Kalimantan, and Sumatra, both from the government and the private sector.</h5>
                                         <br>
                                         <h5 class="mb-0">Jayabaya Technology is trusted because it has a very dedicated and professional team in its field. Our team loves the world of IT so much that we focus on service and development of our products.</h5>
                                         
                                        <div class="facts-wrap2 d-inline-flex w-100">
                                            <div class="fact-box2">
                                                <h6 class="mb-0">Clients</h6>
                                                <span class="text-color2 d-block"><span class="counter">30</span><sup>+</sup></span>
                                            </div>
                                            <div class="fact-box2">
                                                <h6 class="mb-0">Projects</h6>
                                                <span class="text-color2 d-block"><span class="counter">80</span><sup>+</sup></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- About Wrap 5 -->
                    </div>
                </div>
            </section>
            <section>
                <div class="w-100 pt-40 pb-90 bg-color38 position-relative">
                    <div class="container">
                        <div class="service-wrap4 w-100">
                        <center>
                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div>
                                            <h3 class="mb-0">Our Services</h3>
                                            <p class="mb-0">What you can get from our services?</p>
                                </div>
                            </div>
                        </center>
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr1"><i class="metaicon-worldwide"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Website Development</h3>
                                            <p class="mb-0">We are your best choice to help you get website or web apps.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr2"><i class="metaicon-smartphone"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Mobile Apps</h3>
                                            <p class="mb-0">We have good portfolios with IOS and Android apps</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr3"><i class="metaicon-head"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Artificial Intelligence</h3>
                                            <p class="mb-0">AI is our mission to grow up in 4.0 era</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr4"><i class="metaicon-startup"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">IOT Project</h3>
                                            <p class="mb-0">Internet of Things, to coloring your life with technology</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr5"><i class="metaicon-networking"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Network Instalations</h3>
                                            <p class="mb-0">Network make you connected with other. Get our service.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr6"><i class="metaicon-light-bulb"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Electronic Device</h3>
                                            <p class="mb-0">CCTV, Running Text, Sound System and other electronics device.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Services Wrap 4 -->
                    </div>
                </div>
            </section>
@endsection