@extends('layouts.app')
<title>Portfolio - Jayabaya Technology</title>
@section('content')
   
    @php
        $client = DB::select("select * from idclient order by id_client DESC");
    @endphp

            <section>
                <div class="w-100">
                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric125" data-source="gallery" style="background-color:transparent;padding:0px;">
                        <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;background-color: #b8dfff ;background-position: center;background-size: cover;background-repeat: no-repeat;" data-version="5.4.1">
                            <ul>
                                <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/port.png" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['-20','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['20','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp3.png" alt="Slide Mockup 3">
                                    </div>
 -->
                                    <!-- LAYER NR. 4 -->
                                    
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff; ">Portfolio
                                    </div>
                                </li>
                                <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/port.png" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['110','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['180','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-50','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp3.png" alt="Slide Mockup 3">
                                    </div> -->

                                    <!-- LAYER NR. 4 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff;">Portfolio
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!-- END REVOLUTION SLIDER -->
                </div>
            </section>
           <section>
                <div class="w-100 pt-50 pb-90">
                    <center><h2 class="mb-0">Portfolio</h2></center>
                    <div class="port-wrap2 w-100">
                        <div class="row mrg">
                            @foreach ($datas as $data)
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <article class="port-box2 style2 text-center w-100 brd-rd20 position-relative">
                                    <div class="port-thumb2 w-100 overflow-hidden position-relative">
                                        <a href="assets/imagesproject/{{$data->gambar}}" title="" data-fancybox="gallery"><img class="img-fluid w-100" src="assets/imagesproject/{{$data->gambar}}" alt="Portfolio Image 1"></a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                        </div>
                    </div><!-- Portfolio Wrap 2 -->
                </div>
            </section>
@endsection