<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="assets/MetaMax/assets/images/logo.png" sizes="32x32" type="image/png">
 
        <link rel="stylesheet" href="assets/MetaMax/assets/css/all.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/slick.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/style.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/responsive.css">
        <link rel="stylesheet" href="assets/MetaMax/assets/css/color.css">

        <!-- REVOLUTION STYLE SHEETS -->
        <link rel="stylesheet" href="assets/MetaMax/assets/css/revolution/settings.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel="stylesheet" href="assets/MetaMax/assets/css/revolution/layers.css">
        <!-- REVOLUTION NAVIGATION STYLES -->
        <link rel="stylesheet" href="assets/MetaMax/assets/css/revolution/navigation.css">
    </head>
    <body>
        <main>
            <div id="mta-page-loader-container" class="mta-loader-container style1">
                <div id="mta-page-loader" class="mta-loader">
                    <div class="inner"></div>
                    <img class="loader-logo" src="assets/MetaMax/assets/images/logo.gif" alt="Loader Logo" width="200%">
                </div>
            </div>
            <header class="stick style3 logo-center">
                <div class="container d-flex flex-row justify-content-between flex-nowrap">
                    <div class="header-nav-wrap w-100">
                        <div class="menu-overlay position-fixed d-block"></div>
                        <nav class="nav-wrap d-flex flex-row align-items-center justify-content-between flex-nowrap">
                            <div class="menu-wrap d-flex flex-row justify-content-between flex-nowrap w-100">
                                <div class="menu d-flex flex-row align-items-center justify-content-start flex-nowrap w-100">
                                    <ul class="main-menu mb-0 list-unstyled d-flex flex-row align-items-center justify-content-start flex-wrap">
                                        <li class="menu-item-has-children {{(request()->is('/')) ? 'active' : null }}"><a href="/" title="">Home</a>
                                        </li>
                                        <li class="menu-item-has-children {{(request()->is('our-company')) ? 'active' : null }}"><a href="/our-company" title="">Our Company</a>
                                        </li>
                                        <li class="menu-item-has-children {{(request()->is('portfolio')) ? 'active' : null }}"><a href="/portfolio" title="">Portfolio</a>
                                        </li>
                                    </ul>
                                </div><!-- Menu -->
                                <div class="logo text-center">
                                    <a class="desktop-logo" href="/" title="Home">
                                        <img class="default-logo img-fluid" src="assets/MetaMax/assets/images/logo.gif" alt="Logo" srcset="assets/MetaMax/assets/images/logo.gif 2x">
                                    </a>
                                </div><!-- Logo -->
                                <div class="menu d-flex flex-row align-items-center justify-content-end flex-nowrap w-100">
                                    <ul class="main-menu mb-0 list-unstyled d-flex flex-row align-items-center justify-content-start flex-wrap">
                                        <li class="menu-item-has-children {{(request()->is('career')) ? 'active' : null }}"><a href="/career" title="">Career</a>
                                        </li>
                                        <li class="menu-item-has-children {{(request()->is('blog')) ? 'active' : null }}"><a href="/blog" title="">Blogs</a>
                                        </li>
                                        <li class="menu-item-has-children {{(request()->is('contact')) ? 'active' : null }}"><a href="/contact" title="">Contacts</a></li>
                                    </ul>
                                </div><!-- Menu -->
                            </div><!-- Menu Wrap -->
                            <div class="menu-right-icons d-flex flex-row align-items-center justify-content-end flex-nowrap">
                                <span class="search-btn"></span>
                            </div>
                        </nav>
                    </div><!-- Header Nav Wrap -->
                </div>
            </header><!-- Header -->
            <div class="header-search d-flex flex-wrap justify-content-center align-items-center w-100">
                <span class="search-close-btn"><i class="metaicon-cancel-music"></i></span>
                <form>
                    <input type="text" placeholder="Search">
                </form>
            </div><!-- Header Search -->
            <div class="responsive-header w-100" style="background-color: white">
                <div class="responsive-topbar w-100" style="background-color: white">
                    <div class="container d-flex flex-wrap justify-content-between">
                        <div class="logo">
                            <a href="/" title="Home"><img class="img-fluid" src="assets/MetaMax/assets/images/logo.gif" alt="Logo"></a>
                        </div><!-- Logo -->
                        <div class="menu-right-icons text-white d-flex flex-row align-items-center justify-content-end flex-nowrap">
                            <span class="responsive-menu-btn" style="color: #32afff"><i class="fas fa-align-justify"></i></span>
                            <span class="search-btn" style="color: #32afff"></span>
                        </div>
                    </div>
                </div>
                <div class="responsive-menu">
                    <div class="logo d-inline-block w-100">
                        <a href="/" title="Home"><img class="img-fluid" src="assets/MetaMax/assets/images/logo.gif" alt="Logo"></a>
                    </div>
                    <ul class="mb-0 list-unstyled">
                        <li class="{{(request()->is('/')) ? 'active' : null }}"><a href="/" title="">Home</a>
                        </li>
                        <li class="{{(request()->is('our-company')) ? 'active' : null }}"><a href="/our-company" title="">Our Company</a>
                        </li>
                        <li class="{{(request()->is('portfolio')) ? 'active' : null }}"><a href="/portfolio" title="">Portfolio</a>
                        </li>
                        <li class="{{(request()->is('career')) ? 'active' : null }}"><a href="/career" title="">Career</a>
                        <li class="{{(request()->is('blog')) ? 'active' : null }}"><a href="/blog" title="">Blogs</a>
                        </li>
                        <li class="{{(request()->is('contact')) ? 'active' : null }}"><a href="/contact" title="">Contacts</a></li>
                    </ul>
                </div><!-- Responsive Menu -->
            </div><!-- Responsive Header -->

                @yield('content')

            <footer>
                <div class="w-100 pt-70 bg-color28 text-color15 position-relative">
                    <div class="scroll-top-btn position-absolute"><a href="javascript:void(0);" title=""><i class="metaicon-arrow-pointing-to-up"></i></a></div>
                    <div class="container">
                        <div class="footer-wrap d-flex flex-wrap w-100">
                            <div class="footer-about">
                                <div class="logo d-block">
                                    <a title="Home"><img class="img-fluid" src="assets/MetaMax/assets/images/logo.png" alt="Footer Logo" style="max-width: 800%; width: 400%;"></a>
                                </div><!-- Logo -->
                                <p class="mb-0">Jayabaya Technology, Your IT Solutions and Make IT Works Easy</p>
                            </div><!-- Footer About -->
                            <div class="footer-widget-wrap">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-lg-4">
                                        <div class="widget w-100">
                                            <h4 class="text-white">Our Company</h4>
                                            <ul class="mb-0 list-unstyled">
                                                <li><a href="/" title="">Home</a></li>
                                                <li><a href="/our-company" title="">Our Company</a></li>
                                                <li><a href="/portfolio" title="">Portfolios</a></li>
                                                <li><a href="/career" title="">Career</a></li>
                                                <li><a href="/blog" title="">Blogs</a></li>
                                                <li><a href="/contact" title="">Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-4">
                                        <div class="widget w-100">
                                            <h4 class="text-white">Our Services</h4>
                                            <ul class="mb-0 list-unstyled">
                                                <li><a title="">Website Development</a></li>
                                                <li><a title="">Mobile Apps</a></li>
                                                <li><a title="">Artificial Intelligence</a></li>
                                                <li><a title="">IOT Project</a></li>
                                                <li><a title="">Electronic Device</a></li>
                                                <li><a title="">Network Instalations</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-4">
                                        <!--Newslatter Form-->
                                        <div class="widget w-100" id="email-form">
                                            <h4 class="text-white">Subscribe</h4>
                                            <form class="position-relative w-100" method="post" action="#" id="subscribe-form">
                                                <div class="form-group"><div class="response"></div></div>

                                                <input type="email" name="email" class="email" value="" placeholder="Your Email">
                                                <button type="button" id="subscribe-newslatters"><i class="metaicon-send-button"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Footer Wrap -->
                        <div class="footer-content-wrap align-items-center d-flex justify-content-between flex-wrap w-100">
                            <div class="footer-content-inner">
                                <ul class="mb-0 d-flex list-unstyled">
                                    <li class="text-white"><a href="8006000020" title=""><i class="fas fa-phone-alt text-color15"></i>+62 - 858 - 5197 - 1698</a></li>
                                    <li class="text-white"><i class="far fa-map text-color15"></i>Kediri - East Java, Indonesia</li>
                                </ul>
                            </div>
                            <div class="scl-links2">
                                <a href="javascript:void(0);" title="Youtube" target="_blank"><i class="fab fa-youtube"></i></a>
                                <a href="javascript:void(0);" title="Instagram" target="_blank"><i class="fab fa-instagram"></i></a>
                                <a href="javascript:void(0);" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                                <a href="javascript:void(0);" title="Youtube" target="_blank"><i class="fab fa-weixin"></i></a>
                            </div>
                        </div><!-- Footer Content Wrap -->
                        <div class="copyright w-100">
                            <p class="mb-0">Copyright &copy; 2020. All Right Reserved. || PT. Daha Jayabaya Teknologi</p>
                        </div><!-- Copyright -->
                    </div>
                </div>
            </footer>
        </main><!-- Main Wrapper -->

        <script src="assets/MetaMax/assets/js/jquery.min.js"></script>
        <script src="assets/MetaMax/assets/js/popper.min.js"></script>
        <script src="assets/MetaMax/assets/js/bootstrap.min.js"></script>
        <script src="assets/MetaMax/assets/js/bootstrap-select.min.js"></script>
        <script src="assets/MetaMax/assets/js/owl.carousel.min.js"></script>
        <script src="assets/MetaMax/assets/js/scroll-up-bar.min.js"></script>
        <script src="assets/MetaMax/assets/js/jquery.fancybox.min.js"></script>
        <script src="assets/MetaMax/assets/js/counterup.min.js"></script>
        <script src="assets/MetaMax/assets/js/perfect-scrollbar.min.js"></script>
        <script src="assets/MetaMax/assets/js/slick.min.js"></script>
        <script src="assets/MetaMax/assets/js/circle-progress.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
        <script src="assets/MetaMax/assets/js/particles.min.js"></script>
        <script src="assets/MetaMax/assets/js/particle-int.js"></script>
        <script src="assets/MetaMax/assets/js/custom-scripts.js"></script>

        <script src="assets/MetaMax/assets/js/revolution/jquery.themepunch.tools.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/jquery.themepunch.revolution.min.js"></script>

        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.actions.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.migration.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/extensions/revolution.extension.video.min.js"></script>
        <script src="assets/MetaMax/assets/js/revolution/revolution-init3.js"></script>
    </body> 
</html>>
    </body> 
</html>