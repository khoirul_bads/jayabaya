@extends('layouts.app')
<title>Welcome to Jayabaya Technology | Your IT Solutions</title>
@section('content')
   
    @php
        $client = DB::select("select * from idclient order by id_client DESC");
    @endphp

            <section>
                <div class="w-100">
                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric125" data-source="gallery" style="background-color:transparent;padding:0px;">
                        <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;background-color: #b8dfff ;background-position: center;background-size: cover;background-repeat: no-repeat;" data-version="5.4.1">
                            <ul>
                                <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/gif1.gif" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['-20','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['20','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide2-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-2-mckp3.png" alt="Slide Mockup 3">
                                    </div>
 -->
                                    <!-- LAYER NR. 4 -->
                                    
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide2-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff; ">Welcome to <br> Jayabaya Technology
                                    </div>

                                    <!-- LAYER NR. 5 -->
                                    <div class="tp-caption" 
                                        id="slide2-layer-5" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['40','40','40','30']" 
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on" 
                                        data-responsive="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="line-height: 26px;font-family: Rubik;font-size: 1rem;letter-spacing: 0;font-weight: 400;color: #fff;">Your IT Solutions <br> and Make IT Works
                                    </div>

                                    <!-- LAYER NR. 6 -->
                                    <div class="tp-caption rev-btn simple-btn theme-btn fill-btn3" 
                                        id="slide2-layer-6" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['167','150','120','120']" 
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="button" 
                                        data-actions='[{"event":"click","action":"simplelink","slide":"next","delay":"","target": "_self", "url": "javascript:void(0);"}]'
                                        data-responsive_offset="on" 
                                        data-responsive="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[21,17,14,12]"
                                        data-paddingright="[44,40,35,30]"
                                        data-paddingbottom="[21,17,14,12]"
                                        data-paddingleft="[44,40,35,30]"
                                        style="cursor:pointer;line-height: 14px;display: inline-block;font-family: Rubik;font-size: 1rem;letter-spacing: 0;font-weight: 600;">Read More
                                    </div>
                                </li>
                                <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-1" 
                                        data-x="['right','right','right','right']" data-hoffset="['-80','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['30','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:0;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/gif1.gif" alt="Slide Mockup 1">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-2" 
                                        data-x="['right','right','right','right']" data-hoffset="['110','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-40','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2300,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp2.png" alt="Slide Mockup 2">
                                    </div> -->
                                    
                                    <!-- LAYER NR. 3 -->
                                    <!-- <div class="tp-caption tp-resizeme rs-parallaxlevel-3" 
                                        id="slide3-layer-3" 
                                        data-x="['right','right','right','right']" data-hoffset="['180','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-50','0','0','0']" 
                                        data-fontsize="['60','60','50','40']"
                                        data-lineheight="['70','70','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:top;z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        ><img src="assets/MetaMax/assets/images/resources/slide3-3-mckp3.png" alt="Slide Mockup 3">
                                    </div> -->

                                    <!-- LAYER NR. 4 -->
                                    <div class="tp-caption tp-resizeme" 
                                        id="slide3-layer-4" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-80,'-60']" 
                                        data-fontsize="['60','55','50','40']"
                                        data-lineheight="['65','60','60','50']"
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="image" 
                                        data-responsive_offset="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="letter-spacing: 0;font-weight:800;font-family: Nunito;color:#fff;">Build your system. <br> Good Technologies.
                                    </div>

                                    <!-- LAYER NR. 5 -->
                                    <div class="tp-caption" 
                                        id="slide3-layer-5" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['40','40','40','30']" 
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="text" 
                                        data-responsive_offset="on" 
                                        data-responsive="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['left','center','center','center']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        style="line-height: 26px;font-family: Rubik;font-size: 1rem;letter-spacing: 0;font-weight: 400;color: #fff;">Start your bussiness  <br> with our good system
                                    </div>

                                    <!-- LAYER NR. 6 -->
                                    <div class="tp-caption rev-btn simple-btn theme-btn fill-btn3" 
                                        id="slide3-layer-6" 
                                        data-x="['left','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['167','150','120','120']" 
                                        data-width="none"
                                        data-height="none"
                                        data-whitespace="nowrap"                     
                                        data-type="button" 
                                        data-actions='[{"event":"click","action":"simplelink","slide":"next","delay":"","target": "_self", "url": "javascript:void(0);"}]'
                                        data-responsive_offset="on" 
                                        data-responsive="on"
                                        data-frames='[{"from":"y:-[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                        data-textAlign="['center','center','center','center']"
                                        data-paddingtop="[21,17,14,12]"
                                        data-paddingright="[44,40,35,30]"
                                        data-paddingbottom="[21,17,14,12]"
                                        data-paddingleft="[44,40,35,30]"
                                        style="cursor:pointer;line-height: 14px;display: inline-block;font-family: Rubik;font-size: 1rem;letter-spacing: 0;font-weight: 600;">Read More
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div><!-- END REVOLUTION SLIDER -->
                </div>
            </section>
            <section>
                <div class="w-100 pt-70 pb-100 position-relative">
                    <div id="particles6" class="particles-js top-left" data-color="#1ec8bf" data-saturation="300" data-size="40" data-count="8" data-speed="2" data-hide="770" data-image="assets"></div>
                    <div class="container">
                        <div class="about-wrap4 w-100">
                            <div class="row align-items-center">
                                <div class="col-md-12 col-sm-12 col-lg-6 d-none d-lg-block">
                                    <img class="img-fluid" src="assets/MetaMax/assets/images/resources/aboutmockup.gif" alt="About Mockup">
                                </div>
                                <div class="col-md-12 col-sm-12 col-lg-6">
                                    <div class="about-desc4 w-100 px-80">
                                        <h2 class="mb-0 text-color21">Let Us Help <br> You Get More</h2>
                                        <p class="mb-0">Jayabaya Tachnology is a company engaged in IT (Information Technology) and has a home base in Kediri, East Java.</p>
                                        <div class="btns">
                                            <a class="simple-btn theme-btn fill-btn6" href="/our-company" title="Learn More">Learn More</a>
                                            <a class="simple-btn theme-btn bordered-btn3" href="/contact" title="Contact Us">Contact Us</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- About Style 4 -->
                    </div>
                </div>
            </section>
            <section>
                <div class="w-100 pt-40 pb-90 bg-color38 position-relative">
                    <div class="container">
                        <div class="service-wrap4 w-100">
                        <center>
                            <div class="col-md-6 col-sm-6 col-lg-4">
                                <div>
                                            <h3 class="mb-0">Our Services</h3>
                                            <p class="mb-0">What you can get from our services?</p>
                                </div>
                            </div>
                        </center>
                            
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr1"><i class="metaicon-worldwide"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Website Development</h3>
                                            <p class="mb-0">We are your best choice to help you get website or web apps.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr2"><i class="metaicon-smartphone"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Mobile Apps</h3>
                                            <p class="mb-0">We have good portfolios with IOS and Android apps</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr3"><i class="metaicon-head"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Artificial Intelligence</h3>
                                            <p class="mb-0">AI is our mission to grow up in 4.0 era</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr4"><i class="metaicon-startup"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">IOT Project</h3>
                                            <p class="mb-0">Internet of Things, to coloring your life with technology</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr5"><i class="metaicon-networking"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Network Instalations</h3>
                                            <p class="mb-0">Network make you connected with other. Get our service.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="service-box4 mt-50 w-100 d-flex flex-wrap">
                                        <span class="icon-bf-clr6"><i class="metaicon-light-bulb"></i></span>
                                        <div class="service-info4">
                                            <h3 class="mb-0">Electronic Device</h3>
                                            <p class="mb-0">CCTV, Running Text, Sound System and other electronics device.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Services Wrap 4 -->
                    </div>
                </div>
            </section>

            <section>
                <div class="w-100 position-relative pt-120 pb-120">
                    <div class="fixed-bg" style="background-color: white"></div>
                    <div class="container">                        
                        <div class="sec-title2 mb-50 text-center w-100">
                            <div class="d-inline-block">
                                <h2 class="mb-0"><span>Our</span>Clients & Partners</h2>
                            </div>
                        </div><!-- Section Title Style 2 -->
                        <div class="blog-wrap w-100">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-6">
                                    <article class="post-style2 w-100">
                                        <div class="post-img2 w-100 brd-rd20 overflow-hidden position-relative">
                                            <a href="blog-detail-audio.html" title=""><img class="img-fluid w-100" src="assets/MetaMax/assets/images/resources/logoclient.png" alt="Post Image 1"></a>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-md-12 col-sm-12 col-lg-6">
                                    <div class="mini-list-wrap mini-post-caro">
                                        @foreach ($client as $c)
                                        <div class="mini-post-wrap">
                                            <article class="mini-post w-100 d-flex flex-wrap align-items-center">
                                                <div class="min-post-img brd-rd15 overflow-hidden">
                                                    <a href="blog-detail-audio.html" title=""><img class="img-fluid w-75" src="assets/imagesclient/{{$c->client_logo}}" alt="Mini Image 1"></a>
                                                </div>
                                                <div class="mini-post-info">
                                                    <h3 class="mb-0"><a href="blog-detail-audio.html" title="">{{$c->client_name}}</a></h3>
                                                    <p class="mb-0">{{$c->client_location}}</p>
                                                </div>
                                            </article>
                                        </div>
                                        @endforeach
                                    </div><!-- Mini List Wrap -->
                                </div>
                            </div>
                        </div><!-- Blog Wrap -->
                    </div>
                </div>
            </section>

            <section>
                <div class="w-100 pt-155 pb-120 position-relative">
                    <div id="particles8" class="particles-js top-left" data-color="#1ec8bf" data-saturation="300" data-size="40" data-count="8" data-speed="2" data-hide="770" data-image="assets"></div>
                    <div class="container">
                        <div class="seo-mar-frm-wrap w-100">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-sm-12 col-lg-5">
                                    <div class="form-wrap2 w-100 brd-rd20">
                                        <form class="w-100">
                                            <div class="input-field2 brd-rd15 w-100">
                                                <input type="text" placeholder="Name *">
                                                <span class="field-counter">01</span>
                                            </div>
                                            <div class="input-field2 brd-rd15 w-100">
                                                <input type="email" placeholder="Email *">
                                                <span class="field-counter">02</span>
                                            </div>
                                            <div class="input-field2 brd-rd15 w-100">
                                                <input type="tel" placeholder="Phone *">
                                                <span class="field-counter">03</span>
                                            </div>
                                            <div class="form-button2 w-100">
                                                <button class="simple-btn theme-btn fill-btn6 text-center w-100" type="submit">Say Hello</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-lg-7">
                                    <div class="mar-info-wrap w-100">
                                        <h2 class="mb-0 text-color21">Say Hello and Get Our Service</h2>
                                        <ul class="mb-0 list-unstyled w-100 text-color21">
                                            <li>
                                                <i class="metaicon-check-mark"></i>
                                                <p class="mb-0">Fill the all forms on the left side</p>
                                            </li>
                                            <li>
                                                <i class="metaicon-check-mark"></i>
                                                <p class="mb-0">Press Button "Say Hello to Us"</p>
                                            </li>
                                            <li>
                                                <i class="metaicon-check-mark"></i>
                                                <p class="mb-0">Wait until our Marketing Team call you :)</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Seo Marketing Form Wrap -->
                    </div>
                </div>
            </section>

@endsection