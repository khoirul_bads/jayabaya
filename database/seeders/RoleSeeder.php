<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     *
     */

    protected $Roles = [
        [
            'name' => 'SuperAdmin',
            'display_name' => 'SuperAdmin',
            'description' => 'SuperAdmin'
        ],
        [
            'name' => 'Admin',
            'display_name' => 'Admin',
            'description' => 'Admin'
        ],
        [
            'name' => 'User',
            'display_name' => 'User',
            'description' => 'User'
        ],
        [
            'name' => 'Driver',
            'display_name' => 'Driver',
            'description' => 'Driver'
        ],
        [
            'name' => 'Finance',
            'display_name' => 'Finance',
            'description' => 'Finance'
        ],
        [
            'name' => 'Biro',
            'display_name' => 'Biro',
            'description' => 'Biro'
        ],
        [
            'name' => 'Mitra',
            'display_name' => 'Mitra',
            'description' => 'Mart,Food'
        ],
        [
            'name' => 'Website',
            'display_name' => 'Website',
            'description' => 'Website'
        ],

    ];

    public function run()
    {
        foreach ($this->Roles as $role) {
            Role::create($role);
        }
    }
}
