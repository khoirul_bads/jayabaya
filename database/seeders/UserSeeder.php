<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new User();
        $data->name = 'SuperAdmin';
        $data->email = 'SuperAdmin@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('SuperAdmin');

        $data = new User();
        $data->name = 'Admin';
        $data->email = 'Admin@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Admin');

        $data = new User();
        $data->name = 'User';
        $data->email = 'User@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('User');

        $data = new User();
        $data->name = 'Driver';
        $data->email = 'Driver@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Driver');

        $data = new User();
        $data->name = 'Finance';
        $data->email = 'Finance@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Finance');

        $data = new User();
        $data->name = 'Biro';
        $data->email = 'Biro@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Biro');

        $data = new User();
        $data->name = 'Mitra';
        $data->email = 'Mitra@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Mitra');

        $data = new User();
        $data->name = 'Website';
        $data->email = 'Website@email.com';
        $data->password = Hash::make('password');
        $data->save();
        $data->attachRole('Website');
    }
}
